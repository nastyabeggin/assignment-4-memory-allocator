#define _DEFAULT_SOURCE

#include <stdio.h>

#include "test.h"
#include "mem.h"
#include "mem_debug.h"
#include "mem_internals.h"
#include "util.h"

#define MAP_FIXED_NOREPLACE  0x100000

static const size_t INIT_HEAP_SIZE = 12271;

static struct block_header *get_block_header(void *ptr) {
    return (struct block_header *) ((uint8_t *) ptr - offsetof(struct block_header, contents));
}

static void destroy(void *heap, size_t sz) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = sz}).bytes);
}


void *map_pages(void const *addr, size_t length) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE,  MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, -1, 0);
}


static bool test_init_heap(struct block_header **heap, size_t heap_size) {
    *heap = heap_init(heap_size);
    if (!*heap) {
        debug("Failed to init the heap: NULL\n");
        return false;
    }
    debug("Inited heap with size %lu\n", heap_size);
    debug_heap(stderr, *heap);

    return true;
}

static bool test_malloc(void **alloc, size_t alloc_mem) {
    *alloc = _malloc(alloc_mem);
    if (!*alloc) {
        debug("Failed to malloc\n");
        return false;
    }
    struct block_header* block_header = get_block_header(*alloc);
    if (block_header->is_free||block_header->capacity.bytes != alloc_mem) {
        debug("Failed to malloc:\n");
        return true;
    }
    debug("Allocated block with size %lu\n", alloc_mem);
    return true;
}

static bool test_free(void *alloc) {
    _free(alloc);
    if (!get_block_header(alloc)->is_free) {
        debug("Failed to free\n");
        return false;
    }
    debug("Freed block successfully\n");
    return true;
}

static bool test_ordinary_alloc(struct block_header *heap) {
    debug("TEST -- Ordinary memory allocation (1 block).\n");

    const size_t ALLOC_MEM = REGION_MIN_SIZE / 2;
    void *data;
    if (!test_malloc(&data, ALLOC_MEM)) {
        return false;
    }

    debug_heap(stderr, heap);
    if (!test_free(data)) return false;
    debug_heap(stderr, heap);

    return true;
}

static bool test_free_one(struct block_header *heap) {
    debug("Test -- Free one block\n");

    const size_t blocks_size = 3;
    void *blocks[blocks_size];
    for (size_t i = 0; i < blocks_size; i++) {
        if (!test_malloc(blocks + i, heap->capacity.bytes / (blocks_size + i))) {
            return false;
        }
    }
    debug_heap(stderr, heap);
    if (!test_free(blocks[blocks_size / 2])) return false;
    debug_heap(stderr, heap);

    return true;
}

static bool test_free_two(struct block_header *heap) {
    debug("Test -- Free two blocks\n");

    const size_t blocks_size = 5;
    void *blocks[blocks_size];
    for (size_t i = 0; i < blocks_size; i++) {
        if (!test_malloc(blocks + i, heap->capacity.bytes / (blocks_size + i))) {
            return false;
        }
    }
    debug_heap(stderr, heap);
    if (!test_free(blocks[blocks_size / 2])
        || !test_free(blocks[blocks_size / 2 + 1])) {
        return false;
    }
    debug_heap(stderr, heap);

    return true;
}


static bool test_grow_heap(struct block_header *heap) {
    debug("Test -- Memory limit reached. New block replaces old.\n");

    const size_t ALLOC_MEM1 = INIT_HEAP_SIZE*2;
    const size_t ALLOC_MEM2 = INIT_HEAP_SIZE/2;

    void *data1;
    if(!test_malloc(&data1, ALLOC_MEM1)) return false;
    debug_heap(stderr, heap);

    void *data2;
    if(!test_malloc(&data2, ALLOC_MEM2)) return false;
    debug_heap(stderr, heap);

    struct block_header* header_data1 = get_block_header(data1);
    destroy(get_block_header(header_data1), header_data1->capacity.bytes);

    struct block_header* header_data2 = get_block_header(data2);
    destroy(get_block_header(header_data2), header_data2->capacity.bytes);

    return true;
}

static bool test_grow_heap_another_place(struct block_header *first_block) {
    debug("Test -- Allocate memory for region in another place because of lack of memory in heap.\n");

    void *alloc;
    if (!test_malloc(&alloc, INIT_HEAP_SIZE)) return false;
    struct block_header *alloc_header = get_block_header(alloc);

    size_t memory_block_size = REGION_MIN_SIZE * 5;
    void *memory_block_addr =  (alloc_header->contents) + alloc_header->capacity.bytes;
    void *memory_block = map_pages(memory_block_addr, memory_block_size);

    if (memory_block == MAP_FAILED) {
        debug("Failed to set up memory block");
        return false;
    }

    void *data2;
    if (!test_malloc(&data2, INIT_HEAP_SIZE)) {
        return false;
    }
    if(!test_free(data2)) return false;
    struct block_header* header_data2 = get_block_header(data2);
    destroy(get_block_header(header_data2), header_data2->capacity.bytes);
    munmap(memory_block_addr,memory_block_size);
    debug_heap(stderr, first_block);
    return true;
}

static bool (*tests[])(struct block_header *heap) = {
        test_ordinary_alloc,
        test_free_one,
        test_free_two,
        test_grow_heap,
        test_grow_heap_another_place
};

#define TESTS_SIZE 5

#define LINE_SPLITTER "---------------------------------\n"

void test_all() {

    bool all_tests_passed = true;
    for (size_t i = 0; i < TESTS_SIZE; i++) {
        debug("Test %lu started\n", i + 1);
        struct block_header *heap;
        if (test_init_heap(&heap, INIT_HEAP_SIZE) && tests[i](heap)) {
            debug("Test %lu passed successfully\n", i + 1);
        } else {
            debug("Test %lu failed\n", i + 1);
            all_tests_passed = false;
        }
        debug(LINE_SPLITTER);
        destroy(heap, INIT_HEAP_SIZE);
    }
    if (all_tests_passed) {
        debug("All tests passed\n");
    } else {
        debug("Some tests failed\n");
    }
}
